# glforth

A version of Forth written in Gleam, expanded from the [Exercism Forth exercise](https://exercism.org/tracks/gleam/exercises/forth).
