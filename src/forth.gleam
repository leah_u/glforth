import gleam/dict.{type Dict}
import gleam/int
import gleam/io
import gleam/list
import gleam/result
import gleam/string

pub opaque type Forth {
  Forth(
    stack: List(Int),
    words: Dict(String, Word),
    mode: Mode,
    input: List(String),
  )
}

type ForthResult =
  Result(Forth, ForthError)

type Word {
  Word(is_immediate: Bool, action: fn(Forth) -> ForthResult)
}

pub type ForthError {
  DivisionByZero
  StackUnderflow
  InvalidWord
  UnknownWord(word: String)
}

type Mode {
  Interpreting
  Compiling(#(String, List(Word)))
}

pub fn new() -> Forth {
  Forth(stack: [], words: builtins(), mode: Interpreting, input: [])
}

pub fn format_stack(f: Forth) -> String {
  f.stack |> list.reverse |> list.map(int.to_string) |> string.join(" ")
}

pub fn eval(f: Forth, prog: String) -> ForthResult {
  let input = string.split(prog, " ")

  interpret(Forth(..f, input: input))
}

fn interpret(f: Forth) -> ForthResult {
  case f.input {
    [] -> Ok(f)
    [word, ..rest] -> {
      let f = Forth(..f, input: rest)
      case handle_word(f, word) {
        Error(e) -> Error(e)
        Ok(f) -> interpret(f)
      }
    }
  }
}

fn handle_word(f: Forth, word: String) -> ForthResult {
  use word <- result.try(get_word(f, word))
  case f.mode, word.is_immediate {
    Compiling(def), False ->
      Ok(Forth(..f, mode: Compiling(#(def.0, [word, ..def.1]))))
    _, _ -> word.action(f)
  }
}

fn get_word(f: Forth, word: String) -> Result(Word, ForthError) {
  dict.get(f.words, string.uppercase(word))
  |> result.try_recover(fn(_) {
    case int.parse(word) {
      Ok(n) -> Ok(literal(n))
      Error(_) -> Error(UnknownWord(word))
    }
  })
}

fn literal(n: Int) -> Word {
  let action = fn(f: Forth) { Ok(Forth(..f, stack: [n, ..f.stack])) }
  Word(is_immediate: False, action: action)
}

fn builtins() -> Dict(String, Word) {
  [
    #("+", Word(False, binary(int.add))),
    #("-", Word(False, binary(int.subtract))),
    #("*", Word(False, binary(int.multiply))),
    #("/", Word(False, divide)),
    #("DUP", Word(False, dup)),
    #("DROP", Word(False, drop)),
    #("SWAP", Word(False, swap)),
    #("OVER", Word(False, over)),
    #(".", Word(False, dot)),
    #(".S", Word(False, dot_s)),
    #(":", Word(True, start_definition)),
    #(";", Word(True, end_definition)),
  ]
  |> dict.from_list
}

fn dot(f: Forth) -> ForthResult {
  case f.stack {
    [] -> Error(StackUnderflow)
    [val, ..rest] -> {
      io.print(int.to_string(val) <> " ")
      Ok(Forth(..f, stack: rest))
    }
  }
}

fn dot_s(f: Forth) -> ForthResult {
  let stack = format_stack(f)
  let len = list.length(f.stack) |> int.to_string
  io.print("<" <> len <> "> " <> stack <> " ")
  Ok(f)
}

fn end_definition(f: Forth) -> ForthResult {
  case f.mode {
    Interpreting -> Error(InvalidWord)
    Compiling(#(name, def)) ->
      Ok(
        Forth(
          ..f,
          mode: Interpreting,
          words: dict.insert(f.words, string.uppercase(name), user_defined(def)),
        ),
      )
  }
}

fn user_defined(def: List(Word)) -> Word {
  let def = list.reverse(def)
  let action = fn(f: Forth) {
    list.try_fold(def, f, fn(f, word) { word.action(f) })
  }
  Word(is_immediate: False, action: action)
}

fn start_definition(f: Forth) -> ForthResult {
  case f.mode, f.input {
    Compiling(_), _ -> Error(InvalidWord)
    _, [] -> Error(InvalidWord)
    Interpreting, [name, ..rest] ->
      case int.parse(name) {
        Error(_) -> Ok(Forth(..f, input: rest, mode: Compiling(#(name, []))))
        Ok(_) -> Error(InvalidWord)
      }
  }
}

fn binary(op: fn(Int, Int) -> Int) -> fn(Forth) -> ForthResult {
  fn(f: Forth) {
    case f.stack {
      [a, b, ..rest] -> Ok(Forth(..f, stack: [op(b, a), ..rest]))
      _ -> Error(StackUnderflow)
    }
  }
}

fn divide(f: Forth) -> ForthResult {
  case f.stack {
    [a, b, ..rest] -> {
      case a == 0 {
        True -> Error(DivisionByZero)
        False -> Ok(Forth(..f, stack: [b / a, ..rest]))
      }
    }
    _ -> Error(StackUnderflow)
  }
}

fn dup(f: Forth) -> ForthResult {
  case f.stack {
    [a, ..rest] -> Ok(Forth(..f, stack: [a, a, ..rest]))
    _ -> Error(StackUnderflow)
  }
}

fn drop(f: Forth) -> ForthResult {
  case f.stack {
    [_, ..rest] -> Ok(Forth(..f, stack: rest))
    _ -> Error(StackUnderflow)
  }
}

fn swap(f: Forth) -> ForthResult {
  case f.stack {
    [a, b, ..rest] -> Ok(Forth(..f, stack: [b, a, ..rest]))
    _ -> Error(StackUnderflow)
  }
}

fn over(f: Forth) -> ForthResult {
  case f.stack {
    [a, b, ..rest] -> Ok(Forth(..f, stack: [b, a, b, ..rest]))
    _ -> Error(StackUnderflow)
  }
}
