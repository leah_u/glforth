import forth.{type Forth}
import gleam/erlang
import gleam/io
import gleam/string

pub fn main() {
  let forth = forth.new()
  repl(forth)
}

fn repl(f: Forth) {
  case erlang.get_line("> ") {
    Ok("\n") -> repl(f)
    Ok(line) ->
      case forth.eval(f, string.trim(line)) {
        Ok(f) -> {
          io.println("ok")
          repl(f)
        }
        Error(e) -> {
          io.debug(e)
          Nil
        }
      }
    Error(e) -> {
      io.debug(e)
      Nil
    }
  }
}
